import React from "react";
import { Redirect } from 'react-router';


export default class Subject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: undefined,
            selected: []
        };

        this.loadSubject();
    }

    loadSubject() {
        var code = this.props.match.params.code;

        jQuery.ajax({
            url: "/data/" + code + "/_" + code + ".json",
            success: (data) => {
                this.setState({"data": data});
            }
        });
    }

    toggleSelected(i) {
        this.setState((prevState, props) => {
            if (this.isSelected(i)) {
                prevState.selected.splice(prevState.selected.indexOf(i), 1);
            } else {
                prevState.selected.push(i);
            }

            return {
                selected: prevState.selected
            }
        });
    }

    isSelected(i) {
        return this.state.selected.indexOf(i) != -1;
    }

    isSpecial(o) {
        return (o.special === undefined || o.special == false) ? false : true;
    }

    startQuiz() {
        var quizes = this.state.selected.join("+");

        this.props.history.push("/" + this.props.match.params.code + "/" + quizes);
    }

    renderSets() {
        if (this.state.data === undefined)
            return null;

        return (
            <div id="content" className="container subject">
                {Object.entries(this.state.data.datasets).map((val, i) => {
                    return (
                        <div className={"set" + (this.isSelected(val[0]) ? " selected" : "")} key={i} onClick={() => this.toggleSelected(val[0])}>
                            <div className="set__title">{val[0] + ". " + val[1].title}</div>
                            <div className="set__info">
                                <span><b>{(!this.isSpecial(val[1]) ? "" : "*") + val[1].questions}</b> otázok</span>
                            </div>
                        </div>
                    );
                })}
                <div className="startbtn" onClick={() => this.startQuiz()}>Start</div>
            </div>
        );
    }

    render() {
        //console.log(this.state.data);

        if (this.state.data == null || this.state.data.datasets == undefined)
            return null;

        return this.renderSets();
    }
}
