import React from "react";
import update from "immutability-helper";


export default class Homepage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            subjects: {}
        };

        this.loadData();
    }

    loadSubject(code) {
        jQuery.ajax({
            url: "/data/" + code + "/_" + code + ".json",
            success: (data) => {
                this.setState({
                    subjects: update(this.state.subjects, {
                        [code]: {
                            $set: data
                        }
                    })
                });
            }
        });
    }

    loadData() {
        jQuery.ajax({
            url: "/data/data.json",
            success: (data) => {
                data.forEach((el) => {
                    this.loadSubject(el);
                });
            }
        });
    }

    render() {
        return (
            <div id="content" className="homepage">
                {Object.entries(this.state.subjects).map((val, i) => {
                    var total_questions = 0;
                    for (var ds in val[1].datasets) {
                        if (val[1].datasets[ds].special === undefined || val[1].datasets[ds].special === false)
                            total_questions += val[1].datasets[ds].questions;
                    }

                    return (
                        <div className="subject" key={i} onClick={() => this.props.history.push("/" + val[1].code)}>
                            <div className="subject__title"><b>{val[1].code}</b> {val[1].name}</div>
                            <div className="subject__info"><b>{total_questions}</b> otázok</div>
                            <div className="subject__info"><b>{Object.keys(val[1].datasets).length}</b> častí</div>
                        </div>
                    );
                })}
            </div>
        );
    }
}
