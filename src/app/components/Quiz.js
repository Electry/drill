import React from "react";

import Status from "./quiz/Status";
import Question from "./quiz/Question";


export default class Quiz extends React.Component {
    constructor(props) {
        super(props);

        this.vars = {
            // Number of loaded parts
            loaded_count: 0,
            // Number of parts yet to be loaded
            to_load_count: -1
        };

        this.state = {
            // List of all questions, key = ID
            questions: [],

            // ID of currently shown yet to be answered question
            q_current_id: undefined,
            // List of unanswered question IDs
            q_unanswered_ids: [],

            // Counters
            correct_answers_count: 0,
            total_answers_count: 0,
        };

        this.loadSubject();
    }

    loadSubject() {
        var code = this.props.match.params.code;
        var selected_i = this.props.match.params.id.split("+");

        jQuery.ajax({
            url: "/data/" + code + "/_" + code + ".json",
            success: (data) => {
                this.vars.loaded_count = 0;
                this.vars.to_load_count = selected_i.length;

                selected_i.forEach((i) => {
                    this.loadQuestions(data.datasets[i].data);
                });
            }
        });
    }

    loadQuestions(set) {
        var code = this.props.match.params.code;

        jQuery.ajax({
            url: "/data/" + code + "/" + set,
            success: (data) => {
                this.vars.loaded_count++;
                var new_questions = [...this.state.questions, ...data];
                var new_ids = Object.keys(new_questions);

                this.setState(prevState => ({
                    questions: new_questions,
                    q_unanswered_ids: new_ids,
                    q_current_id: this.getRandomQuestionId(new_ids)
                }));
            }
        });
    }

    getRandomQuestionId(from = this.state.q_unanswered_ids) {
        return from[Math.round(Math.random() * (from.length - 1))];
    }

    nextQuestion(answered_correctly) {
        this.setState((prevState, props) => {
            // Correct/Incorrect counter
            if (answered_correctly) {
                prevState.correct_answers_count++;
                prevState.q_unanswered_ids.splice(prevState.q_unanswered_ids.indexOf(prevState.q_current_id), 1);
            }
            prevState.total_answers_count++;

            // New question
            prevState.q_current_id = this.getRandomQuestionId(prevState.q_unanswered_ids);

            return {
                q_current_id:          prevState.q_current_id,
                q_unanswered_ids:      prevState.q_unanswered_ids,
                correct_answers_count: prevState.correct_answers_count,
                total_answers_count:   prevState.total_answers_count,
            }
        });
    }

    render() {
        // If is still loading
        if (this.vars.loaded_count != this.vars.to_load_count) {
            return (
                <div id="content" className="quiz">
                    <div className="notice">Loading!</div>
                </div>
            );
        }

        // Loaded, but no questions
        if (Object.keys(this.state.questions).length == 0) {
            return (
                <div id="content" className="quiz">
                    <div className="notice">No questions found!</div>
                </div>
            );
        }

        // Get next question
        var question = <Question nextCallback={(a) => this.nextQuestion(a)}
               question={this.state.questions[this.state.q_current_id]}/>

        var end = <div className="notice">Konec!</div>;

        return (
            <div id="content" className="quiz">
                <Status q_unanswered={this.state.q_unanswered_ids.length}
                       q_total   = {this.state.questions.length}
                       a_correct = {this.state.correct_answers_count}
                       a_total   = {this.state.total_answers_count}/>
                {(this.state.q_current_id == null || this.state.q_current_id == undefined) ?
                    end : question}
                <div className="clearboth"></div>
            </div>
        );
    }
}
