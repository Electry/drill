import React from "react";

export default class Question extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            answered_wrong: [],
            answered_correct: []
        };

        this.shuffleAnswers(this.props);
    }

    answerSingle(i) {
        if (!this.isMultichoice() && this.state.answered_correct.length !== 0)
            return;

        if (this.isMultichoice() &&
                (this.state.answered_correct.includes(i) || this.state.answered_wrong.includes(i)))
            return;

        if (this.isAnswerCorrect(i)) {
            // Render colored
            this.setState((prevState, props) => {
                prevState.answered_correct.push(i);
                return {
                    answered_correct: prevState.answered_correct
                };
            });
        } else {
            // Render colored
            this.setState((prevState, props) => {
                prevState.answered_wrong.push(i);
                return {
                    answered_wrong: prevState.answered_wrong
                };
            })
        }
    }

    shuffleAnswers(nextProps = this.props) {
        var keys = Object.keys(nextProps.question.answers);
        for (var k in keys) {
            var i = keys[k];
            var j = keys[Math.round(Math.random() * (keys.length - 1))];

            // Update correct answer
            if (this.isAnswerCorrect(i, nextProps)) {
                if (this.isMultichoice(nextProps)) {
                    // If both are correct. no need to do anything here, otherwise swap correct indexes
                    if (!this.isAnswerCorrect(j, nextProps)) {
                        // Remove swapped id from correct answers
                        nextProps.question.correct.splice(nextProps.question.correct.indexOf(i), 1, j);
                    }
                } else {
                    nextProps.question.correct = j;
                }
            } else if (this.isAnswerCorrect(j, nextProps)) {
                // Same deal here
                if (this.isMultichoice(nextProps)) {
                    if (!this.isAnswerCorrect(i, nextProps)) {
                        nextProps.question.correct.splice(nextProps.question.correct.indexOf(j), 1, i);
                    }
                } else {
                    nextProps.question.correct = i;
                }
            }

            // Swap answers
            var tmp = nextProps.question.answers[j];
            nextProps.question.answers[j] = nextProps.question.answers[i];
            nextProps.question.answers[i] = tmp;

            //[this.props.question.answers[i], this.props.question.answers[j]] =
            //    [this.props.question.answers[j], this.props.question.answers[i]];
        }
    }

    isAnswerCorrect(i, nextProps = this.props) {
        if (this.isMultichoice(nextProps))
            return nextProps.question.correct.includes(i);

        return nextProps.question.correct == i;
    }

    getAnswers() {
        return (
            Object.entries(this.props.question.answers).map((val, i) => {
                var cl = "question__answer";
                if (this.state.answered_wrong.includes(val[0]))
                    cl += " wrong";
                else if (this.state.answered_correct.includes(val[0])) {
                    cl += " correct";
                }
                return <div data-key={val[0]} key={i}
                            onClick={() => this.answerSingle(val[0])}
                            className={cl}
                            dangerouslySetInnerHTML={{__html: val[1]}}></div>
            })
        );
    }

    areAnsweredAll() {
        if (!this.isMultichoice())
            return this.state.answered_correct.includes(this.props.question.correct);

        var answered = [...this.state.answered_correct, ...this.state.answered_wrong];
        return this.props.question.correct.every(id => answered.includes(id));
    }

    isMultichoice(nextProps = this.props) {
        return Array.isArray(nextProps.question.correct);
    }

    nextQuestion() {
        if (!this.areAnsweredAll()) {
            return;
        }

        var is_answered_wrongly = this.state.answered_wrong.length > 0;
        this.props.nextCallback(!is_answered_wrongly);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            answered_correct: [],
            answered_wrong: []
        });

        this.shuffleAnswers(nextProps);
    }

    render() {
        return (
            <div className="question">
                <h2 className="question__title" dangerouslySetInnerHTML={{__html: this.props.question.text.replace(/\n/g, "<br/>")}}></h2>
                <div className="question__answers">
                    {this.getAnswers()}
                </div>
                <div className={"question__next active"} onClick={() => this.nextQuestion()}>Next</div>
            </div>
        );
    }
}
