import React from "react";

export default class Status extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var answered = this.props.q_total - this.props.q_unanswered;
        var incorrect = this.props.a_total - this.props.a_correct;
        var percentage = Math.round((this.props.a_correct / this.props.a_total) * 100);

        return (
            <div className="status">
                <div className="sprogress">
                    {"Otázka " + ((answered + 1) > this.props.q_total ? answered : (answered + 1)) + " z " + this.props.q_total}
                </div>
                <div className="sratio">
                    {this.props.a_correct}:<span className="wrong">{incorrect} </span>
                    ~{percentage}%
                </div>
            </div>
        );
    }
}
