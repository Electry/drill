import React from "react";

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="footer">
                <span>v{__VERSION__} - {__BUILD__} - {(typeof __PRODUCTION__ !== "undefined") ? "production" : "development"}</span>
            </div>
        );
    }
}
