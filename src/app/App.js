import React from "react";
import { Route, Switch } from 'react-router-dom'

import Menu from "./Menu";
import Footer from "./Footer";

import Homepage from "./components/Homepage";
import About from "./components/About";
import Subject from "./components/Subject";
import Quiz from "./components/Quiz";

export default class App extends React.Component {
    render() {
        return (
            <div>
                <Menu/>
                <Switch>
                    <Route exact path='/' component={Homepage}/>
                    <Route exact path='/about' component={About}/>
                    <Route exact path='/:code' component={Subject}/>
                    <Route path='/:code/:id' component={Quiz}/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}
