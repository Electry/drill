// ReactJS
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

// jQuery
import jQuery from "jquery";
window.jQuery = jQuery;

// Bootstrap v4
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

// Analytics
import ReactGA from "react-ga";
ReactGA.initialize("UA-118754673-2");
ReactGA.pageview(window.location.pathname + window.location.search);

// App
import App from "app/App";
import Style from "style.less";
import ".htaccess"

var app = document.getElementById("app");

ReactDOM.render((
    <BrowserRouter>
        <App/>
    </BrowserRouter>
), app)
