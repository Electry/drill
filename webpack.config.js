const webpack           = require("webpack");
const path              = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    devtool: "inline-source-map",
    devServer: {
        host: "0.0.0.0",
        port: 8080,
        historyApiFallback: true,
        contentBase: "./",
        overlay: {
            warnings: true,
            errors: true
        }
    },
    context: path.resolve(__dirname, "src"),
    entry: [
        "webpack-dev-server/client?http://127.0.0.1:8080",
        "webpack/hot/only-dev-server",
        "./"
    ],
    output: {
        path: path.join(__dirname, "public"),
        filename: "bundle.js",
        publicPath: "/",
    },
    resolve: {
        modules: ["node_modules", "src"],
        extensions: [".js"]
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [
                "react-hot-loader/webpack",
                "babel-loader?presets[]=react,presets[]=env"
            ]
        }, {
            test: /\.less$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: ["css-loader", "less-loader"]
            })
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })
        }, {
            test: /\.(png|jp(e*)g|svg)$/,
            use: {
                loader: "url-loader",
                options: {
                    name: "img/[name].[hash:8].[ext]",
                    limit: 512
                },
            },
        }, {
            test: /\.htaccess$/,
            use: {
                loader: "file-loader",
                options: {
                    name: ".htaccess"
                },
            },
        }]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin({
            filename: "styles.css",
            allChunks: true
        }),
        new HtmlWebpackPlugin({
            template: "index.html"
        }),
        new CopyWebpackPlugin([
            { from: "data/", to: "data/", ignore: [".git/**/*"] },
        ]),
        new webpack.DefinePlugin({
            __VERSION__: JSON.stringify(require("./package.json").version),
            __BUILD__: JSON.stringify((new Date()).toUTCString())
        })
    ]
}
